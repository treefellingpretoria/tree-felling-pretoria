![tree.jpg](https://bitbucket.org/repo/xkqAMj/images/674365711-tree.jpg)
Tree Felling Services

Tree Felling Services offer a diverse range of tree services including Removal, Felling, Lopping, Trimming, Pruning, Shaping, Surgery, Arborist Services, Transplanting, Woodchipping, Stump Removal, Land Clearing. We also offer Firewood and Mulch products for sale.Well cared for and maintained trees provide shade and privacy for your family, while adding interest to your landscape. They also enhance the value of your property, in fact, combined with landscaping they can add as much as 10% to your property value. Proper care and maintenance will help insure that your trees continue to be an asset to you and your property for years to come.

The professionals at Centurion Tree Felling will give you the right advice and work with you to provide the best solution at affordable rates.

Boomsloping

Boomsloping Pretoria / Tree Felling Pretoria. Beware of cheap quotes the contractors may not have insurance for the amenity tree industry to protect you and your property.The Company is insured for the amenity tree industry and our qualified arborists have extensive experience in the lopping, removal and pruning of trees.

The Centurion Tree Felling Company will offer you:
• Boomsloping
• Stump removal
• Palm leaf trimming
• Palm removal
• Trimming and pruning
• Precision cutting
• Site clearance
• Garden cleaning
• Rubble removal

## Copyright and License

Copyright 2013-2015 Centurion Tree Felling, LLC. Code released under the [Apache 2.0](http://www.centuriontreefelling.co.za/services) license.